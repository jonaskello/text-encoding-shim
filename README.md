# Text Encoding Shim
When I was looking for a simple lightweight polyfill that works great with TypeScript I was not happe with the results.
While a couple polyfills exist, they either broke in my special setup and lead to unexpected results or they implemented a bunch
of features that I did not even want to have. So I wrote my own shim.

## Installation
### npm
Get it via *npm* by adding `text-encoding-shim` to your `package.json` or run:
```shell
npm install text-encoding-shim
```

### Bower
Get it via *Bower* by adding it to your `bower.json` or run:
```shell
bower install --save text-encoding-shim
```

### HTML script tag
Altenatively you can simply download this project folder from [Gitlab](https://gitlab.com/PseudoPsycho/text-encoding-shim)
and add it to your html script tags like so:
```shell
<script type="text/javascript" src="text-encoding-shim/index.js"></script>
```

### TypeScript definitions
If you are using TypeScript you do not need to create a definition file. This project already includes one.
If you are still using *typings* you may need to run this command to copy the file:
```shell
typings install --save --global npm:text-encoding-shim
```

## Importing the polyfill
This polyfill utilizes the Universal Module Definition (UMD) and be used with either a module loader or standalone.
If you import it by adding a script tag you do not have to do anything else. It will automatically be bound to the global scope.
### CommonJS
```js
var TextEncodingShim = require('text-encoding-shim');
var TextEncoder = TextEncodingShim.TextEncoder;
var TextDecoder = TextEncodingShim.TextDecoder;
```

### Asynchronous Module Definition (AMD)
```js
define([TextEncodingShim], function() {
	//...
});
```

### ES2015 or TypeScript
```js
import { TextEncoder, TextDecoder } from 'text-encoding-shim';
```

## Basic Usage
```js
var uint8array = new TextEncoder('utf-8').encode(string);
var string = new TextDecoder('utf-8').decode(uint8array);
```

## Limitations
If a native implementation is available it will always be handled preferred to this polyfill.
Just like native implementations of Mozilla Firefox or Google Chrome, this library only supports UTF-8 encoding.
This makes it so fast. If you need additional encodings, check out [this more advanced polyfill](https://github.com/inexorabletash/text-encoding).